package cz.hrabe.menu;

import android.view.View;

public class MenuItem {
    final int iconResId;
    final String title;
    final int layoutResId;
    final ItemSetter itemSetter;

    public interface ItemSetter {
        void setUp(View v, MenuAdapter.MenuViewHolder viewHolder);
    }
    public MenuItem(int iconResId, String title){
        this(iconResId, title, 0, null);
    }

    public MenuItem(int iconResId, String title, int layoutResId, ItemSetter itemSetter){
        this.iconResId = iconResId;
        this.title = title;
        this.layoutResId = layoutResId;
        this.itemSetter = itemSetter;
    }
}
