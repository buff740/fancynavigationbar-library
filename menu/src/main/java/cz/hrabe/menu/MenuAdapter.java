package cz.hrabe.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {
    private static final int IMPLICIT_LAYOUT = R.layout.implicit_menu_item;
    private final Context context;
    private final int layoutResId;
    private ArrayList<MenuItem> items = new ArrayList<>();

    public MenuAdapter(Context context) {
        this(context, IMPLICIT_LAYOUT);
    }

    public MenuAdapter(Context context, int layoutResId) {
        super();
        this.context = context;
        this.layoutResId = layoutResId;

        View v = LayoutInflater.from(context).inflate(layoutResId, null, false);
        v.findViewById(R.id.title);
    }

    public void addItem(MenuItem item) {
        items.add(item);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(layoutResId, parent, false);
        return new MenuViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        ((ViewGroup) holder.itemView.findViewById(R.id.content)).removeAllViews();
        MenuItem item = items.get(position);
        holder.title.setText(item.title);
        holder.icon.setImageResource(item.iconResId);
        if (item.layoutResId != 0) {
            ViewGroup v = (ViewGroup) LayoutInflater.from(context).inflate(item.layoutResId, null, false);
            item.itemSetter.setUp(v, holder);
            ((ViewGroup) holder.itemView.findViewById(R.id.content)).addView(v);
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull MenuViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView title;

        public MenuViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            title = itemView.findViewById(R.id.title);
        }
    }
}
