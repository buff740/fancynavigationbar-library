package cz.hrabe.fancynavigationbar;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;

import static android.graphics.Color.parseColor;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static android.os.Build.VERSION_CODES.N;
import static android.os.Build.VERSION_CODES.N_MR1;

final class ThemeUtils {

    // material_deep_teal_500
    static final int FALLBACK_COLOR = parseColor("#009688");

    private ThemeUtils() {
        // no instances
    }

    static boolean isAtLeastL() {
        return SDK_INT >= N_MR1;
    }

    @TargetApi(N_MR1)
    static int resolvePrimaryColor(Context context) {
        Theme theme = context.getTheme();

        // on Lollipop, grab system colorAccent attribute
        // pre-Lollipop, grab AppCompat colorAccent attribute
        // finally, check for custom mp_colorAccent attribute
        int attr = isAtLeastL() ? android.R.attr.colorPrimary : R.attr.colorPrimary;
        TypedArray typedArray = theme.obtainStyledAttributes(new int[] { attr, R.attr.colorPrimary });

        int accentColor = typedArray.getColor(0, FALLBACK_COLOR);
        accentColor = typedArray.getColor(1, accentColor);
        typedArray.recycle();

        return accentColor;
    }

    @TargetApi(N_MR1)
    static int resolvePrimaryDarkColor(Context context) {
        Theme theme = context.getTheme();

        // on Lollipop, grab system colorAccent attribute
        // pre-Lollipop, grab AppCompat colorAccent attribute
        // finally, check for custom mp_colorAccent attribute
        int attr = isAtLeastL() ? android.R.attr.colorPrimaryDark : R.attr.colorPrimaryDark;
        TypedArray typedArray = theme.obtainStyledAttributes(new int[] { attr, R.attr.colorPrimaryDark });

        int accentColor = typedArray.getColor(0, FALLBACK_COLOR);
        accentColor = typedArray.getColor(1, accentColor);
        typedArray.recycle();

        return accentColor;
    }

    @TargetApi(N_MR1)
    static int resolveSecondaryColor(Context context) {
        Theme theme = context.getTheme();

        // on Lollipop, grab system colorAccent attribute
        // pre-Lollipop, grab AppCompat colorAccent attribute
        // finally, check for custom mp_colorAccent attribute
        int attr = isAtLeastL() ? android.R.attr.colorSecondary : R.attr.colorSecondary;
        TypedArray typedArray = theme.obtainStyledAttributes(new int[] { attr, R.attr.colorSecondary });

        int accentColor = typedArray.getColor(0, FALLBACK_COLOR);
        accentColor = typedArray.getColor(1, accentColor);
        typedArray.recycle();

        return accentColor;
    }
}
