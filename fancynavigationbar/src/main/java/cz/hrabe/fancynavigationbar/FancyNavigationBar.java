package cz.hrabe.fancynavigationbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FancyNavigationBar extends LinearLayout {
    /**
     * Tells the adapter to stretch views if the number of views in a single panel is less than the maximum number
     */
    private final boolean stretchViews;

    private static final int IMPLICIT_BACKGROUND_COLOR = Color.rgb(0,0,0);

    private final int styleid;

    private final FancyNavigationAdapter adapter = new FancyNavigationAdapter(3);
    private final ArrayList<NavigationBarItem> items = new ArrayList<>();

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

    }

    public static class NavigationBarItem {
        private final String text;
        private final int imageResId;
        private final OnClickListener listener;

        public NavigationBarItem(String text, int imageResId, OnClickListener listener) {
            this.text = text;
            this.imageResId = imageResId;
            this.listener = listener;
        }

        public NavigationBarItem(String text, OnClickListener listener) {
            this(text, 0, listener);
        }
    }

    public FancyNavigationBar(Context context) {
        this(context, null);
    }

    public FancyNavigationBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FancyNavigationBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public FancyNavigationBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FancyNavigationBar, defStyleAttr, defStyleRes);

        int backgroundColor = a.getInt(R.styleable.FancyNavigationBar_backgroundColor, IMPLICIT_BACKGROUND_COLOR);
        stretchViews = a.getBoolean(R.styleable.FancyNavigationBar_stretchItems, false);
        styleid = a.getResourceId(R.styleable.FancyNavigationBar_buttonStyle, R.style.button);

        a.recycle();

        inflate(context, R.layout.navigation_bar_horizontal, this);
        RecyclerView list = findViewById(R.id.list);

        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(list);
        list.addItemDecoration(new LinePagerIndicatorDecoration());

        list.setBackgroundColor(backgroundColor);
        findViewById(R.id.fillView).setBackgroundColor(backgroundColor);
    }

    public void setHomeButton(NavigationBarItem item) {
        ((ConstraintLayout) findViewById(R.id.homeButtonPanel)).setVisibility(VISIBLE);
        ContextThemeWrapper wrapper = new ContextThemeWrapper(getContext(), styleid);
        Button homeButton = new Button(wrapper, null, 0);
        homeButton.setText(item.text);
        homeButton.setCompoundDrawablesWithIntrinsicBounds(0, item.imageResId, 0, 0);
        homeButton.setOnClickListener(item.listener);
        LayoutParams homeButtonLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        homeButtonLayoutParams.leftMargin = 4;
        homeButton.setLayoutParams(homeButtonLayoutParams);

        ((ConstraintLayout) findViewById(R.id.homeButtonPanel)).addView(homeButton);
    }

    public void addItem(NavigationBarItem item) {
        items.add(item);
        adapter.notifyDataSetChanged();
    }

    public void addItems(NavigationBarItem[] items) {
        for (NavigationBarItem item : items) {
            addItem(item);
        }
    }

    private class FancyNavigationAdapter extends RecyclerView.Adapter<FancyNavigationAdapter.FancyNavigationVievHolder> {
        private final int itemsNumberOnSlide;

        FancyNavigationAdapter(int itemsNumberOnSlide) {
            this.itemsNumberOnSlide = itemsNumberOnSlide;
        }

        @NonNull
        @Override
        public FancyNavigationVievHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.horizontal_list_item, parent, false);
            return new FancyNavigationVievHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull FancyNavigationVievHolder holder, int position) {
            ((LinearLayout) holder.itemView).removeAllViews();
            int startIndex = (itemsNumberOnSlide * position);
            for (int i = startIndex; i < startIndex + itemsNumberOnSlide; i++) {
                if (i == items.size()) {
                    break;
                }
                NavigationBarItem item = items.get(i);

                ViewGroup.LayoutParams layoutParams = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);

                ContextThemeWrapper wrapper = new ContextThemeWrapper(getContext(), styleid);
                Button button = new Button(wrapper, null, 0);

                button.setText(item.text);
                button.setLayoutParams(layoutParams);
                button.setOnClickListener(item.listener);
                button.setCompoundDrawablesWithIntrinsicBounds(0, item.imageResId, 0, 0);

                ((LinearLayout) holder.itemView).addView(button);
            }
        }

        @Override
        public int getItemCount() {
            return (int) Math.ceil(items.size() / (double) itemsNumberOnSlide);
        }

        public class FancyNavigationVievHolder extends RecyclerView.ViewHolder {

            public FancyNavigationVievHolder(@NonNull View itemView) {
                super(itemView);
                if (!stretchViews) {
                        ((LinearLayout) itemView).setWeightSum(itemsNumberOnSlide);
                }
            }
        }
    }
}
