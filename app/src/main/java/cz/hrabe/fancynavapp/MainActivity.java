package cz.hrabe.fancynavapp;


import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cz.hrabe.fancynavigationbar.FancyNavigationBar;
import cz.hrabe.menu.MenuAdapter;
import cz.hrabe.menu.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FancyNavigationBar.NavigationBarItem[] testItems = {
                new FancyNavigationBar.NavigationBarItem("TEST 1", null)
                , new FancyNavigationBar.NavigationBarItem("TEST 2", null)
                , new FancyNavigationBar.NavigationBarItem("TEST 3", null)
                , new FancyNavigationBar.NavigationBarItem("TEST 4", null)};

        FancyNavigationBar navigationBar = findViewById(R.id.navBar);
        navigationBar.setHomeButton(new FancyNavigationBar.NavigationBarItem("home", null));
        navigationBar.addItems(testItems);

        RecyclerView menuList = findViewById(R.id.menuList);
        menuList.setLayoutManager(new LinearLayoutManager(getBaseContext()));

        MenuAdapter adapter = new MenuAdapter(getBaseContext(), R.layout.menu_item);
        MenuItem updateMenuItem = new MenuItem(R.drawable.ic_update, "Data", R.layout.update, null);
        adapter.addItem(updateMenuItem);

        menuList.setAdapter(adapter);

    }
}